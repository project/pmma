
Page manager menu aliases
-----------------
by Sander Goossens, sander@codebrothrs.be

Page manager menu aliases allows you to set aliases for your page directly from
the Page manager configuration.

This module can be used in the following scenario:

- You create a new page with path /faq.
- You want to add this page to the menu in different places.
- You add the page under Contact and under About us.
- When you go to Contact and then to faq you will see that the active trail
  is not correct.
- This is because the menu system of drupal takes the first occurrence of a
  menu item with the path /faq.

This module provides you to enter multiple aliases for your page. Then you can
change the path of faq under contact to one of your aliases.
Now the active trail will be correct!
